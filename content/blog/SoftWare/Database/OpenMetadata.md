---
title: "OpenMetadata"
date: 2024-04-15T22:12:56+09:00
---

# OpenMetadata

## 概要

データカタログの一種。

データリネージの自動化や多数のデータソースとの統合が用意な特徴がある

## クイックスタート

以下の情報を参照

https://docs.open-metadata.org/v1.3.x/quick-start/local-docker-deployment

## 使用方法

基本的なOpenMetaDataのガイドは以下

https://docs.open-metadata.org/v1.3.x/how-to-guides/guide-for-data-users

