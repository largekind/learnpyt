---
title: "Elasticsearch"
date: 2024-04-15T21:55:21+09:00
---

# Elasticsearch

## 概要

大容量データを処理するのを想定した全文検索エンジン

[リンク](https://www.elastic.co/jp/)

AmudsenやOpenMetadataといったデータカタログに使われている他、企業としても内部で使用している実例があるほど有名なもの
