---
title: "API Economy"
date: 2024-03-25T21:19:46+09:00
draft: True
categories: ["InfoEngineer"]
tags: ["InfoEngineer", "Software"]
---
# API Economy

## 概要

APIを公開することで、他社サービスも活用し広がっていく経済圏のこと

具体的にはGoogleマップのAPIなどを活用する等があげられる
