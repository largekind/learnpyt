---
title: "ITILサービス"
date: 2023-04-05T00:00:00+09:00
tags: ["InfoEngineer", "ProjectManagement"]
categories: ["InfoEngineer"]
---
# ITILサービス

ITサービスマネジメントにおいて、それぞれの組織に適用した優れた取り組みを体系化したガイドライン

これを活用することにより、自社や同業者が成功している他社と比較・評価しギャップを埋めることができる