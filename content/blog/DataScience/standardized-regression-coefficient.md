---
title: "Standardized Regression Coefficient"
date: 2024-03-06T21:27:32+09:00
categories: ["DataScience"]
tags: ["DataScience"]
---
# Standardized Regression Coefficient

## 概要

標準偏回帰係数

重回帰分析における説明変数の回帰変数のことを指す
